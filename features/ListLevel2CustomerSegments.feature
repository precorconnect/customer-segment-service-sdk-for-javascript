Feature: List Level 2 Customer Segments
  Lists all level 2 customer segments

  Scenario: Success
    Given I provide a valid accessToken
    When I execute listLevel2CustomerSegments
    Then all level 2 customer segments in the customer-segment-service are returned