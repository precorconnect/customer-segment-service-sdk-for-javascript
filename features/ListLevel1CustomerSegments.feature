Feature: List Level 1 Customer Segments
  Lists all level 1 customer segments

  Scenario: Success
    Given I provide a valid accessToken
    When I execute listLevel1CustomerSegments
    Then all level 1 customer segments in the customer-segment-service are returned