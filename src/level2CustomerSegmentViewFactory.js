import Level1CustomerSegmentView from './level1CustomerSegmentView';
import Level2CustomerSegmentView from './level2CustomerSegmentView';

export default class Level2CustomerSegmentViewFactory {

    static construct(data):Level2CustomerSegmentView {

        const level1CustomerSegment =
            new Level1CustomerSegmentView(
                data.level1CustomerSegment.id,
                data.level1CustomerSegment.name
            );

        return new Level2CustomerSegmentView(
            data.id,
            data.name,
            level1CustomerSegment
        );

    }

}